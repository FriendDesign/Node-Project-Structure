'use strict'

require('dotenv').config();

var databaseConfig = require('./components/database');
var logConfig = require('./components/logger');
var serverConfig = require('./components/server');
var secretKeyConfig = require('./components/secretkey');

module.exports = Object.assign({}, databaseConfig, logConfig, serverConfig, secretKeyConfig);