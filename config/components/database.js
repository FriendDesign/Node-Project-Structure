'use strict'

var joi = require('joi');

const envVarsSchema = joi.object({
    DB_HOST: joi.string().default("localhost"),
    DB_PORT: joi.string().default("27017"),
    DB_USERNAME: joi.string().required(),
    DB_PASSWORD: joi.string().required()
}).unknown().required();

const {
    error,
    value: envVars
} = joi.validate(process.env, envVarsSchema);

if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}

const config = {
    database: {
        DB_HOST: envVars.DB_HOST,
        DB_PORT: envVars.DB_PORT,
        DB_USERNAME: envVars.DB_USERNAME,
        DB_PASSWORD: envVars.DB_PASSWORD
    }
}

module.exports = config;