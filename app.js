var express = require('express');
var bodyParser = require('body-parser');
var router = require('./router');
var middleWare = require('./middleware');
var {
    server: serverConfig
} = require('./config');
var logger = require('./logger');
var app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());
app.use('/user', router.user);
app.use('*', router.unknowRequest);
app.use(middleWare.serverError);
app.listen(serverConfig.PORT, function (error) {
    if (error) {
        return logger.error(`Can not start server on port: ${serverConfig.PORT}, message: ${error.message}`);
    }
    return logger.info(`Server running on port: ${serverConfig.PORT}`);
});