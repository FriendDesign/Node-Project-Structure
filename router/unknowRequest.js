var route = require('express').Router();

route.get('*', function (req, res, next) {
    var err = new Error('Request not found');
    err.status = 404;
    next(err);
});

module.exports = route;