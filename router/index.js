var userRouter = require('./user');
var unknowRequestRouter = require('./unknowRequest');

module.exports = {
    user: userRouter,
    unknowRequest: unknowRequestRouter
}